import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import contactObject from '@salesforce/schema/Contact';
import contactFName from '@salesforce/schema/Contact.FirstName';
import contactLName from '@salesforce/schema/Contact.LastName';
import contactEmail from '@salesforce/schema/Contact.Email';
export default class contactCreator extends LightningElement{
	objectApiName = contactObject;
	objectFields = [contactFName, contactLName, contactEmail];
	handleSuccess(event) {
        const toastEvent = new ShowToastEvent({
            title: "Contact created",
            message: "Record ID: " + event.detail.id,
            variant: "success"
        });
        this.dispatchEvent(toastEvent);
    }
}